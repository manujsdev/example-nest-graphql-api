## Description

Api Graphql for Aqui Hay app

## Configuration
The .env file must be created in the root of the project, with the following content:<br>
DATABASE_USER = databaseuser<br>
DATABASE_PASSWORD = databasepassword<br>
DATABASE_HOST = localhost<br>
DATABASE_PORT = 5432<br>
DATABASE_DATABASE = databasename<br>
PORT = 3001<br>
MODE = DEV<br>
RUN_MIGRATIONS = true<br>
SYNCHRONIZE = false<br>
<br>
The ormconfig.json file must be created in the root of the project, with the following content:<br>
```json
  "type": "postgres",
  "host": "locahost",
  "port": 5432,
  "username": "username",
  "password": "password",
  "database": "databasename",
  "entities": ["dist/**/*.entity{.ts,.js}"],
  "migrationsTableName": "migrations",
  "migrations": ["dist/migrations/*.js"],
  "cli": {
    "migrationsDir": "src/migrations"
  },
  "synchronize": true
```

## Installation

```bash
$ **npm install** or **yarn install**
```

## Running the app

```bash
# development
$ **npm run start** or **yarn start**

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ **npm run test** or **yarn test**

# test coverage
$ npm run test:cov

# run linter
$ **npm run lint** or **yarn lint**
```

## License

  Nest is [MIT licensed](LICENSE).
