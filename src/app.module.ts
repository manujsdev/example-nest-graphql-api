import {Module} from '@nestjs/common';
import {GraphQLModule} from '@nestjs/graphql';
import {FoobarModule} from './foobar/foobar.module';
// import {ConfigModule} from './config/config.module';
import {TypeOrmModule} from '@nestjs/typeorm';
import {configService} from './config/config.service';
import {UploadScalar} from './shared/scalars/upload.scalar';
// import { graphqlUploadExpress } from 'graphql-upload';
import {GraphQLUpload} from 'graphql-upload';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
      resolvers: {Upload: GraphQLUpload},
      uploads: {
        maxFileSize: 10000000, // 10 MB
        maxFiles: 5,
      },
    }),
    TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
    FoobarModule,
    // UploadScalar,
    // ConfigModule,
  ],
})
export class AppModule {}
