import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import * as helmet from 'helmet';
import * as cors from 'cors';
import * as rateLimit from 'express-rate-limit';
import {configService} from 'src/config/config.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(helmet());
  app.use(cors());
  app.use(
    rateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 100, // limit each IP to 100 requests per windowMs
    }),
  );
  await app.listen(configService.getPort());
}
bootstrap();
