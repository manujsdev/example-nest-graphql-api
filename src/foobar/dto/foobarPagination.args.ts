import {ArgsType} from 'type-graphql';
import {BasePaginationArgs} from '../../shared/dto/basePagination.args';

@ArgsType()
export class FoobarPaginationArgs extends BasePaginationArgs {
  // Redefine here
}
