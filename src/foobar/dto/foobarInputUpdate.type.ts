import {Field, InputType} from 'type-graphql';
import {Foobar} from '../entities/foobar.entity';

@InputType({description: 'Foobar data to update'})
export class FoobarInputUpdateType implements Partial<Foobar> {
  // code here
  @Field({nullable: true})
  firstName: string;

  @Field({nullable: true})
  lastName: string;
}
