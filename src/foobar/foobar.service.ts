import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Foobar} from './entities/foobar.entity';
import {FoobarRepository} from './repositories/foobar.respository';
import {FoobarPaginationArgs} from './dto/foobarPagination.args';

@Injectable()
export class FoobarService {
  private readonly foobars: Foobar[];

  constructor(@InjectRepository(FoobarRepository) private readonly repo: FoobarRepository) {}

  async findPaginateFoobars(foobarArgs: FoobarPaginationArgs): Promise<any[]> {
    try {
      return await this.repo.getAllPaginateFoobar(foobarArgs);
    } catch (e) {
      throw new Error(`Error in Service to Find all foobars: ${e}`);
    }
  }

  async findOneFoobar(foobarId: number): Promise<Foobar> {
    try {
      return await this.repo.findOne(foobarId);
    } catch (e) {
      throw new Error(`Error in Service to Find foobar with id: ${foobarId}. ${e}`);
    }
  }

  addFoobar = async (foobarInput: Foobar): Promise<Foobar> => {
    try {
      return await this.repo.addFoobar(foobarInput);
    } catch (e) {
      throw new Error(`Error in Service to add foobar: ${e}`);
    }
  };

  updateFoobar = async (id: number, foobarInput: Foobar): Promise<Foobar> => {
    try {
      const obj = await await this.repo.findOne({where: {id}});
      return this.repo.save({
        ...obj, // existing fields
        ...foobarInput, // updated fields
      });
    } catch (e) {
      throw new Error(`Error in Service to update foobar: ${e}`);
    }
  };

  deleteFoobar = async (id: number): Promise<boolean> => {
    try {
      const obj = await await this.repo.delete(id);
      return obj.affected === 1 ? true : false;
    } catch (e) {
      throw new Error(`Error in Service to delete foobar: ${e}`);
    }
  };
}
