import {Args, Resolver, Query, Mutation, Context} from '@nestjs/graphql';
import {Arg, Int, Ctx} from 'type-graphql';
// import {FileInterceptor} from '@nestjs/platform-express';
// import {UseInterceptors} from '@nestjs/common';
import {FoobarService} from './foobar.service';
import {Foobar} from './entities/foobar.entity';
import {FoobarPaginationArgs} from './dto/foobarPagination.args';
import {createBaseResolver} from '../shared/resolvers/base.resolver';
import {FoobarPaginatedResponse} from './dto/foobarPaginatedResponse';
import {FoobarInputType} from './dto/foobarInput.type';
import {FoobarInputUpdateType} from './dto/foobarInputUpdate.type';
import {GraphQLUpload} from 'graphql-upload';

const FoobarBaseResolver = createBaseResolver('foobar', Foobar);

@Resolver(of => Foobar)
export class FoobarResolver extends FoobarBaseResolver {
  constructor(private readonly foobarService: FoobarService) {
    super();
  }

  // Return the Foobar List paginated
  @Query(returns => FoobarPaginatedResponse, {
    nullable: true,
    description: 'Get data for all foobars from database, with paginate.',
  })
  async foobarList(@Args() foobarArgs: FoobarPaginationArgs): Promise<FoobarPaginatedResponse> {
    try {
      const foobars = await this.foobarService.findPaginateFoobars(foobarArgs);
      const count = foobars[0].length;
      return {
        items: foobars[0],
        count,
        total: foobars[1],
      };
    } catch (e) {
      throw new Error(`Error in Resolver to Find all foobars: ${e}`);
    }
  }

  // Return the Foobar
  @Query(returns => Foobar, {nullable: true, description: 'Get data foobar by id.'})
  async foobarGet(@Args({name: 'id', type: () => Int}) id: number): Promise<Foobar> {
    try {
      return await this.foobarService.findOneFoobar(id);
    } catch (e) {
      throw new Error(`Error in Resolver to Find one foobar: ${e}`);
    }
  }

  // Add one Foobar and return the Foobar
  @Mutation(returns => Foobar, {nullable: true, description: 'Add a foobar and return the object.'})
  // @UseInterceptors(FileInterceptor('file'))
  async foobarAdd(
    @Args('input') foobarInput: FoobarInputType,
    @Arg('file', type => GraphQLUpload, {nullable: true}) file: any,
  ): Promise<Foobar> {
    try {
      return await this.foobarService.addFoobar(foobarInput);
    } catch (e) {
      throw new Error(`Error in Resolver to add foobar: ${e}`);
    }
  }

  // Update one Foobar and return the Foobar
  @Mutation(returns => Foobar, {nullable: true, description: 'Update a foobar and return the object.'})
  async foobarUpdate(
    @Args({name: 'id', type: () => Int}) id: number,
    @Args('input') foobarInput: FoobarInputUpdateType,
  ): Promise<Foobar> {
    try {
      return await this.foobarService.updateFoobar(id, foobarInput);
    } catch (e) {
      throw new Error(`Error in Resolver to update foobar: ${e}`);
    }
  }

  // Delete one Foobar and return true or false
  @Mutation(returns => Boolean, {nullable: true, description: 'Update a foobar and return the object.'})
  async foobarDelete(@Args({name: 'id', type: () => Int}) id: number): Promise<boolean> {
    try {
      return await this.foobarService.deleteFoobar(id);
    } catch (e) {
      throw new Error(`Error in Resolver to dalete foobar: ${e}`);
    }
  }
}
