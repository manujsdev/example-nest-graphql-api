import {Test, TestingModule} from '@nestjs/testing';
import {TypeOrmModule} from '@nestjs/typeorm';
import {FoobarResolver} from './foobar.resolver';
import {FoobarService} from './foobar.service';
import {FoobarRepository} from './repositories/foobar.respository';

describe('FoobarResolver', () => {
  let resolver: FoobarResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FoobarResolver, FoobarService, FoobarRepository],
    }).compile();

    resolver = module.get<FoobarResolver>(FoobarResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
