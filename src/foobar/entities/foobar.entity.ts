import {Field, ObjectType} from 'type-graphql';
import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm';
import {BaseEntity} from '../../shared/entities/base.entity';

@ObjectType({description: 'Object representing a Foobar'})
@Entity()
export class Foobar extends BaseEntity {
  @Field({nullable: true})
  @Column()
  firstName: string;

  @Field({nullable: true})
  @Column()
  lastName: string;
}
